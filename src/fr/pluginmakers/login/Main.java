package fr.pluginmakers.login;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	public static Set<String> logging = new HashSet<String>();
	public static Set<String> moving  = new HashSet<String>();
	
	public String locale = "en";
	
	static Logger log = Logger.getLogger("minecraft");

	File configfile;
    FileConfiguration config;
    
    public static String mysql_server          = null;
	public static String mysql_port            = null;
	public static String mysql_database        = null;
	public static String mysql_user            = null;
	public static String mysql_password        = null;
	public static String database_table        = null;
	public static String database_id           = null;
	public static String database_nick         = null;
	public static String database_password     = null;
	public static String hashs_crypt           = null; // plain/md5/sha1
	public static String name                  = null;
    public static String data_loading          = null;
    public static String data_loading_error    = null;
    public static String mysqli_loading        = null;
    public static String mysqli_loading_error  = null;
    public static String player_login          = null;
    public static String player_loginweb       = null;
    public static String player_action         = null;
    public static String player_serveur        = null;
    public static String player_cmd            = null;
    public static String player_log            = null;
    public static String player_pass           = null;
    public static String player_passerror      = null;

	public void onEnable(){
        //_______________________________________________________________conf
        if(setupConf()) {
        		log.info(name + " " + data_loading);
        } else {
        		log.info(name + " " + data_loading_error);
                setEnabled(false);
                return;
        }
        //_______________________________________________________________mysqli
        String mysql = null;
		try {  mysql = Mysqli.Verif_Mysqli(); } catch (ClassNotFoundException e) { e.printStackTrace(); } catch (SQLException e) { e.printStackTrace(); }  
		if (!mysql.equals("0")) {
				log.info(name + " " + mysqli_loading);
		} else {
				log.info(name + " " + mysqli_loading_error);
				setEnabled(false);
				return;
		}
		//_____________________________________________________________cmds&events
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);
		getCommand("login").setExecutor(new LoginCommand());
	}
 
	public void onDisable(){
		for(Player p : Bukkit.getOnlinePlayers()){
			int x = p.getLocation().getBlockX();
			int z = p.getLocation().getBlockZ();
			World w = p.getLocation().getWorld();
			Location loc = new Location(w, x, p.getLocation().getWorld().getHighestBlockYAt(x, z), z);
			p.teleport(loc);
		}
	}
	
    private boolean setupConf() {
    	if (!getDataFolder().exists()) {
             getDataFolder().mkdir();
    	}
    	configfile = new File(getDataFolder() + File.separator + "config.yml");
    	if(!configfile.exists()) {
    		config = YamlConfiguration.loadConfiguration(configfile);	
    		config.set("mysql.server",          "localhost");
    		config.set("mysql.port",            "3306");
    		config.set("mysql.database",        "base");
    		config.set("mysql.user",            "username");
    		config.set("mysql.password",        "password");
    		config.set("database.table",        "mumber");  
    		config.set("database.id",           "id");  
    		config.set("database.nick",         "nick");  
    		config.set("database.password",     "pass");  
    		config.set("hashs.crypt",           "plain");
    		config.set("language.lang",         "en");
    		config.set("language.name",         "[PMLogin]");
    		config.set("language.english.data-loading",        "Configuration loaded successfully !");
    		config.set("language.english.data-loadingerror",   "Unable to load the configuration !");
    		config.set("language.english.mysqli-loading",      "Mysqli configuration loaded successfully !");
    		config.set("language.english.mysqli-loadingerror", "Unable to load configuration Mysqli !");
    		config.set("language.english.player-login",        "Please login with /login password");
    		config.set("language.english.player-loginweb",     "Please register on the website www.*******. com !");
    		config.set("language.english.player-action",       "Not possible, please login !");
    		config.set("language.english.player-serveur",      "This command can only be used in the game !");
    		config.set("language.english.player-cmd",          "Please use: /login password");
    		config.set("language.english.player-log",          "You are already connected to the server !");
    		config.set("language.english.player-pass",         "You are now connected. Good game !");
    		config.set("language.english.player-passerror",    "Incorrect password");
    		config.set("language.french.data-loading",         "Configuration chargé avec succés !");
    		config.set("language.french.data-loadingerror",    "Impossible de charger la configuration !");
    		config.set("language.french.mysqli-loading",       "Configuration Mysqli chargé avec succés !");
    		config.set("language.french.mysqli-loadingerror",  "Impossible de charger la configuration Mysqli !");
    		config.set("language.french.player-login",         "Veuillez vous connecter avec /login password");
    		config.set("language.french.player-loginweb",      "Veuillez vous inscrire sur le site www.*******.com !");
    		config.set("language.french.player-action",        "Action impossible, veuillez vous connecter !");
    		config.set("language.french.player-serveur",       "Cette commande est uniquement utilisable en jeu !");
    		config.set("language.french.player-cmd",           "Veuillez utilisez : /login password");
    		config.set("language.french.player-log",           "Vous êtes déjà connecté sur le serveur !");
    		config.set("language.french.player-pass",          "Vous êtes désormais connecté. Bon jeu !");
    		config.set("language.french.player-passerror",     "Mot de passe incorrect");
            try {
                    config.save(configfile);
            } catch (IOException e) {
                    e.printStackTrace();
                    return false;
            }
    	}
    	config = YamlConfiguration.loadConfiguration(configfile);
        loadConf();
        return true;
    }
    
    private void loadConf() {
    	try {
    		mysql_server       = config.getString("mysql.server");
    		mysql_port         = config.getString("mysql.port");
    		mysql_database     = config.getString("mysql.database");
    		mysql_user         = config.getString("mysql.user");
    		mysql_password     = config.getString("mysql.password");
    		database_table     = config.getString("database.table");
    		database_id        = config.getString("database.id");
    		database_nick      = config.getString("database.nick");
    		database_password  = config.getString("database.password");
    		hashs_crypt        = config.getString("hashs.crypt");
    		locale             = config.getString("language.lang");
    		name               = config.getString("language.name");
            if (locale.equalsIgnoreCase("en")) {
            			data_loading           = config.getString("language.english.data-loading");
            			data_loading_error     = config.getString("language.english.data-loadingerror");
            			mysqli_loading         = config.getString("language.english.mysqli-loading");
            			mysqli_loading_error   = config.getString("language.english.mysqli-loadingerror");
            			player_login           = config.getString("language.english.player-login");
            			player_loginweb        = config.getString("language.english.player-loginweb");
            			player_action          = config.getString("language.english.player-action");	
            			player_serveur         = config.getString("language.english.player-serveur");
            			player_cmd             = config.getString("language.english.player-cmd");
            			player_log             = config.getString("language.english.player-log");
            			player_pass            = config.getString("language.english.player-pass");
            			player_passerror       = config.getString("language.english.player-passerror");	
            } else if (locale.equalsIgnoreCase("fr")) {
    					data_loading           = config.getString("language.french.data-loading");
    					data_loading_error     = config.getString("language.french.data-loadingerror");
    					mysqli_loading         = config.getString("language.french.mysqli-loading");
    					mysqli_loading_error   = config.getString("language.french.mysqli-loadingerror");
    					player_login           = config.getString("language.french.player-login");
    					player_loginweb        = config.getString("language.french.player-loginweb");
    					player_action          = config.getString("language.french.player-action");	
    					player_serveur         = config.getString("language.french.player-serveur");
    					player_cmd             = config.getString("language.french.player-cmd");
    					player_log             = config.getString("language.french.player-log");
    					player_pass            = config.getString("language.french.player-pass");
    					player_passerror       = config.getString("language.french.player-passerror");
            } else {
            			data_loading           = config.getString("language.english.data-loading");
            			data_loading_error     = config.getString("language.english.data-loadingerror");
            			mysqli_loading         = config.getString("language.english.mysqli-loading");
            			mysqli_loading_error   = config.getString("language.english.mysqli-loadingerror");
            			player_login           = config.getString("language.english.player-login");
            			player_loginweb        = config.getString("language.english.player-loginweb");
            			player_action          = config.getString("language.english.player-action");	
            			player_serveur         = config.getString("language.english.player-serveur");
            			player_cmd             = config.getString("language.english.player-cmd");
            			player_log             = config.getString("language.english.player-log");
            			player_pass            = config.getString("language.english.player-pass");
            			player_passerror       = config.getString("language.english.player-passerror");	
            }
    	} catch(Exception e) {
            config = null;
            configfile.delete();
            setupConf();
            return;
    	}	
    	if (!config.contains("mysql.server")   || !config.contains("mysql.port")    || !config.contains("mysql.database") || !config.contains("mysql.user")        || !config.contains("mysql.password") ||
    		!config.contains("database.table") || !config.contains("database.id")   || !config.contains("database.nick")  || !config.contains("database.password") || 
    		!config.contains("hashs.crypt")    || !config.contains("language.lang") || !config.contains("language.name")) {	
            config = null;
            configfile.delete();
            setupConf();
            return;
    	}	
    }
 
}
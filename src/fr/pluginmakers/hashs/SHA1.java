package fr.pluginmakers.hashs;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA1 {

	public static String sha1(String password) throws NoSuchAlgorithmException {
		String original = password;
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.update(original.getBytes());
		byte[] digest = md.digest();
		StringBuffer sb = new StringBuffer();
		for (byte b : digest) {
			sb.append(Integer.toHexString((int) (b & 0xff)));
		}
		return sb.toString();
    }

}
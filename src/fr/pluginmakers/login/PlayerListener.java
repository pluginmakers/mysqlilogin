package fr.pluginmakers.login;

import java.sql.SQLException;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent event) throws InterruptedException, SQLException {
		Player p = event.getPlayer();
		int x = p.getLocation().getBlockX();
		int z = p.getLocation().getBlockZ();
		World w = p.getLocation().getWorld();
		Location loc = new Location(w, x, p.getLocation().getWorld().getHighestBlockYAt(x, z), z);
		p.teleport(loc);
		String verif_register = Mysqli.verif_register(p.getName());
		if (verif_register != null) {
			p.sendMessage(ChatColor.GRAY + Main.name + " " + ChatColor.DARK_RED + Main.player_login);
		} else {
			p.sendMessage(ChatColor.GRAY + Main.name + " " + ChatColor.DARK_RED + Main.player_loginweb);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerQuit(PlayerQuitEvent e) {
		Player p   = e.getPlayer();
		Main.logging.remove(p.getName());
		if (Main.moving.contains(p.getName())) { 
			Main.moving.remove(p.getName()); 
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		if (!Main.logging.contains(p.getName())) {
			e.setTo(e.getFrom());
			Action_Error(p);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		if (!Main.logging.contains(p.getName())) {
			e.setCancelled(true);
			Action_Error(p);
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onBlockPlace(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		if (!Main.logging.contains(p.getName())) {
			e.setCancelled(true);
			Action_Error(p);
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onBlockBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		if (!Main.logging.contains(p.getName())) {
			e.setCancelled(true);
			Action_Error(p);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerDropItem(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
		if (!Main.logging.contains(p.getName())) {
			e.setCancelled(true);
			Action_Error(p);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerPickupItem(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
		if (!Main.logging.contains(p.getName())) {
			e.setCancelled(true);
			Action_Error(p);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void OnHealthRegain(EntityRegainHealthEvent e) {
		Entity entity = e.getEntity();
		if(!(entity instanceof Player)) {
			return;
		}
		Player p = (Player)entity;
		if (!Main.logging.contains(p.getName())) {
			e.setCancelled(true);
			Action_Error(p);
		}	
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void OnFoodLevelChange(FoodLevelChangeEvent e) {
		Entity entity = e.getEntity();
		if(!(entity instanceof Player)) {
			return;
		}
		Player p = (Player)entity;
		if (!Main.logging.contains(p.getName())) {
			e.setCancelled(true);
			Action_Error(p);
		}	
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onInventoryClick(InventoryClickEvent e) {
		Entity entity = e.getWhoClicked();
		if(!(entity instanceof Player)) {
			return;
		}
		Player p = (Player)entity;
		if (!Main.logging.contains(p.getName())) {
			e.setCancelled(true);
			Action_Error(p);
		}	
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		Entity defender = e.getEntity();
		Entity damager = e.getDamager();	
		if(defender instanceof Player) {
			Player p1 = (Player) defender;
			if (!Main.logging.contains(p1.getName())) {
				e.setCancelled(true);
				Action_Error(p1);
				return;
			}
			if(damager instanceof Player) {
				Player p2 = (Player) damager;
				if (!Main.logging.contains(p2.getName())) {
					e.setCancelled(true);
					Action_Error(p2);
					return;
				}	
			}	
		}	
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent e) {
		Player p = e.getPlayer();
		if (!Main.logging.contains(p.getName())) {
			if(!e.getMessage().startsWith("/login")) {
				e.setCancelled(true);
				Action_Error(p);
			}
		}
	}
	
	public static void Action_Error(Player p) {
		if (!Main.moving.contains(p.getName())) {
			p.sendMessage(ChatColor.GRAY + Main.name + " " + ChatColor.DARK_RED + Main.player_action);
			Main.moving.add(p.getName());
		}
	}
	
}
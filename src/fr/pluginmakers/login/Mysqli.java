package fr.pluginmakers.login;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Mysqli {

	private static Connection connect                  = null;
	private static Statement statement                 = null;
	private static PreparedStatement preparedStatement = null;
	private static ResultSet resultSet                 = null;
	private static String verif                        = "0";
	static String data0;
	
	static String mysql = "jdbc:mysql://" + Main.mysql_server + ":" + Main.mysql_port + "/" + Main.mysql_database + "?" + "user=" + Main.mysql_user + "&password=" + Main.mysql_password;
	static String driver = "org.bukkit.craftbukkit.libs.org.gjt.mm.mysql.Driver";
	
	public static String Verif_Mysqli() throws ClassNotFoundException, SQLException {
		Class.forName(driver);
		try {
			connect = DriverManager.getConnection(mysql);
			verif = "1";
		} catch (SQLException e) { /*e.printStackTrace();*/ } finally { close(); }
		return verif;
	}
	
	public static String execute(String query) throws SQLException {
		try {
			Class.forName(driver);
			connect = DriverManager.getConnection(mysql);
			preparedStatement = connect.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				data0 = resultSet.getString("id");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			close();
		}
		return data0;
	}
	
	public static String verif_register(String pseudo) throws SQLException {
		   String verif_register = execute("SELECT " + Main.database_id + "," + Main.database_nick + ", COUNT(*) FROM " + Main.database_table + " WHERE " + Main.database_nick + "='" + pseudo + "'");
        return verif_register;
	}
	
	public static String verif_password(String pseudo, String password) throws SQLException {
		   String verif_password = execute("SELECT " + Main.database_id + "," + Main.database_nick + "," + Main.database_password + ", COUNT(*) FROM " + Main.database_table + " WHERE " + Main.database_nick + "='" + pseudo + "' AND " + Main.database_password + "='" + password + "'");
        return verif_password;
	}
	
	public static void close() throws SQLException {
			if (resultSet != null) { resultSet.close(); }
			if (statement != null) { statement.close(); }
			if (connect   != null) { connect.close();   }
	}
	
}
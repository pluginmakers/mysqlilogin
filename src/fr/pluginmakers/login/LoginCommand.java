package fr.pluginmakers.login;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import fr.pluginmakers.hashs.MD5;
import fr.pluginmakers.hashs.SHA1;

public class LoginCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {
		Player p = (Player) sender;
		if (!(sender instanceof Player)) {
			Main.log.info(ChatColor.RED + Main.player_serveur);
			return true;
		}
		if (args.length == 0) {
			p.sendMessage(ChatColor.GRAY + Main.name + " " + ChatColor.DARK_RED + Main.player_cmd);
			return true;
		}
		if (Main.logging.contains(p.getName())) {
			p.sendMessage(ChatColor.GRAY + Main.name + " " + ChatColor.DARK_RED + Main.player_log);
			return true;
		}
		String verif_password = null;
		try {
			if (Main.hashs_crypt.equals("plain")) {
				verif_password = Mysqli.verif_password(p.getName(),args[0]);
			}
			if (Main.hashs_crypt.equals("md5")) {
				String pass = MD5.md5(args[0]);
				verif_password = Mysqli.verif_password(p.getName(),pass);
			}
			if (Main.hashs_crypt.equals("sha1")) {
				String pass = SHA1.sha1(args[0]);
				verif_password = Mysqli.verif_password(p.getName(),pass);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		if (verif_password != null) {
			Main.logging.add(p.getName());
			p.sendMessage(ChatColor.GRAY + Main.name + " " + ChatColor.DARK_GREEN + Main.player_pass);
		} else {
			p.sendMessage(ChatColor.GRAY + Main.name + " " + ChatColor.DARK_RED + Main.player_passerror);
		}
		return true;
	}

}